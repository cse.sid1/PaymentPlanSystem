package com.demo.paymentPlan.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class PaymentPlanAppException extends RuntimeException{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The code. */
    protected int code;
    
    public PaymentPlanAppException() {
		super();
	}
	/**
     * Instantiates a new PaymentPlanAppException exception. 
     * @param message the message 
     * @param code the code
     */
    public PaymentPlanAppException(String message, int code) {
        super(message);
        this.code = code;
    }
    /**
     * Instantiates a new PaymentPlanAppException exception.
     * @param cause the cause 
     * @param code the code
     */
    public PaymentPlanAppException(Throwable cause, int code) {
        super(cause);
        this.code = code;
    }
    /**
     * Instantiates a new PaymentPlanAppException exception.
     * @param message the message
     * @param cause the cause
     * @param code the code
     */
    public PaymentPlanAppException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }
    /**
     * Instantiates a new PaymentPlanAppException exception.
     * @param code the code
     */
    public PaymentPlanAppException(int code) {
        super();
        this.code = code;
    }
    /**
     * Gets the code.
     * @return the code
     */
    public int getCode() {
        return code;
    }
    /** * Sets the code. * * @param code the new code */
    public void setCode(int code) {
        this.code = code;
    }

}
