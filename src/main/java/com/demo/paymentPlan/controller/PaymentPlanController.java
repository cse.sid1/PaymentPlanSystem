package com.demo.paymentPlan.controller;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.paymentPlan.exception.PaymentPlanAppException;
import com.demo.paymentPlan.model.PaymentPlanRequest;
import com.demo.paymentPlan.model.PaymentPlanResponse;
import com.demo.paymentPlan.services.PaymentPlanService;


@RestController
public class PaymentPlanController {

	@Autowired
	private PaymentPlanService paymentPlanService;
	
	@RequestMapping(value = "/createPaymentPlan", method = RequestMethod.POST)
	public Long createPaymentPlan(@RequestBody PaymentPlanRequest paymentPlanRequest) throws PaymentPlanAppException{
		
		return paymentPlanService.insertPaymentPlanDetails(paymentPlanRequest);	
	}
	
	@RequestMapping(value = "/getPaymentPlanByPaymentId/{paymentId}", method = RequestMethod.GET)
	public PaymentPlanResponse getPaymentPlanByPaymentId(@PathVariable BigInteger paymentId) throws PaymentPlanAppException{
		
		return paymentPlanService.getPaymentPlanDetailsById(paymentId);
	}
	
	@RequestMapping(value = "/getAllPaymentPlans", method = RequestMethod.GET)
	public List<PaymentPlanResponse> getAllPaymentPlans() throws PaymentPlanAppException{
		
		return paymentPlanService.getAllPaymentPlansDetail();
	}
}
