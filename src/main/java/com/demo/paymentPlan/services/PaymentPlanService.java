package com.demo.paymentPlan.services;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.paymentPlan.entity.PaymentPlanEntity;
import com.demo.paymentPlan.exception.PaymentPlanAppException;
import com.demo.paymentPlan.model.PaymentPlanRequest;
import com.demo.paymentPlan.model.PaymentPlanResponse;
import com.demo.paymentPlan.repositories.PaymentPlanRepository;
import com.demo.paymentPlan.repositories.PaymentPlanRepositoryImpl;

@Service
public class PaymentPlanService {

	@Autowired
	private PaymentPlanRepository paymentPlanRepository;
	
	@Autowired
	private PaymentPlanRepositoryImpl paymentPlanRepositoryImpl;
	
	
	public Long insertPaymentPlanDetails(PaymentPlanRequest paymentPlanRequest) throws PaymentPlanAppException{
		
		PaymentPlanEntity paymentPlanEntity = new PaymentPlanEntity();
		Double totalAmount = paymentPlanRequest.getTotalAmount();
		Long noOfPayments = paymentPlanRequest.getNumberOfPayments();
		
		Double lastAmount = null;
		
		Double regularPayment = totalAmount / noOfPayments;
		Double regularPayment1 = regularPayment*100;
		Double wholeNoCheck = regularPayment1 % 1;
		
		DecimalFormat decimalFormat = new DecimalFormat("#.##");

		if(wholeNoCheck != 0) {
			regularPayment = new Double(decimalFormat.format(regularPayment));
			Double preTotalAmount = regularPayment * (noOfPayments -1);
			lastAmount = totalAmount - preTotalAmount;
			paymentPlanEntity.setLastAmount(new Double(decimalFormat.format(lastAmount)));
		}
		
		paymentPlanEntity.setTotalAmount(totalAmount);
		paymentPlanEntity.setNumberOfPayments(noOfPayments);
		paymentPlanEntity.setRegularPaymentAmount(regularPayment);
		
		 
		paymentPlanRepositoryImpl.insertPaymentPlan(paymentPlanEntity);
		
		return null;
	}
	
	public PaymentPlanResponse getPaymentPlanDetailsById(BigInteger paymentId) throws PaymentPlanAppException{
		PaymentPlanResponse paymentPlanResponse = new PaymentPlanResponse();
		
		PaymentPlanEntity paymentPlanEntity = paymentPlanRepository.getPaymentPlanById(paymentId);
		
		paymentPlanResponse.setPaymentId(paymentPlanEntity.getPaymentId());
		paymentPlanResponse.setTotalAmount(paymentPlanEntity.getTotalAmount());
		paymentPlanResponse.setNumberOfPayments(paymentPlanEntity.getNumberOfPayments());
		paymentPlanResponse.setRegularPaymentAmount(paymentPlanEntity.getRegularPaymentAmount());
		paymentPlanResponse.setLastAmount(paymentPlanEntity.getLastAmount());
		
		return paymentPlanResponse;
	}
	
	public List<PaymentPlanResponse> getAllPaymentPlansDetail() throws PaymentPlanAppException{
		
		PaymentPlanResponse paymentPlanResponse = null;
		List<PaymentPlanResponse> listPaymentPlanResponse = new ArrayList<PaymentPlanResponse>();
		List<PaymentPlanEntity> listPaymentPlanEntity = paymentPlanRepository.getAllPaymentPlans();
		
		for(PaymentPlanEntity paymentPlanEntity : listPaymentPlanEntity) {
			paymentPlanResponse = new PaymentPlanResponse();
			paymentPlanResponse.setPaymentId(paymentPlanEntity.getPaymentId());
			paymentPlanResponse.setTotalAmount(paymentPlanEntity.getTotalAmount());
			paymentPlanResponse.setNumberOfPayments(paymentPlanEntity.getNumberOfPayments());
			paymentPlanResponse.setRegularPaymentAmount(paymentPlanEntity.getRegularPaymentAmount());
			paymentPlanResponse.setLastAmount(paymentPlanEntity.getLastAmount());
			listPaymentPlanResponse.add(paymentPlanResponse);
		}
		return listPaymentPlanResponse;
	}
	
}
