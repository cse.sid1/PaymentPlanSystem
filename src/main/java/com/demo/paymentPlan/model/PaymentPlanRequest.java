package com.demo.paymentPlan.model;

import java.io.Serializable;

public class PaymentPlanRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	private Double totalAmount;
	private Long numberOfPayments;
	/**
	 * @return the totalAmount
	 */
	public Double getTotalAmount() {
		return totalAmount;
	}
	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 * @return the numberOfPayments
	 */
	public Long getNumberOfPayments() {
		return numberOfPayments;
	}
	/**
	 * @param numberOfPayments the numberOfPayments to set
	 */
	public void setNumberOfPayments(Long numberOfPayments) {
		this.numberOfPayments = numberOfPayments;
	}
	
	
}
