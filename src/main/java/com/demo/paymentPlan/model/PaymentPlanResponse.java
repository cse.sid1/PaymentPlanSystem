package com.demo.paymentPlan.model;

import java.io.Serializable;
import java.math.BigInteger;

public class PaymentPlanResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	private BigInteger paymentId;
	private Double totalAmount;
	private Long numberOfPayments;
	private Double regularPaymentAmount;
	private Double lastAmount;
	/**
	 * @return the paymentId
	 */
	public BigInteger getPaymentId() {
		return paymentId;
	}
	/**
	 * @param paymentId the paymentId to set
	 */
	public void setPaymentId(BigInteger paymentId) {
		this.paymentId = paymentId;
	}
	/**
	 * @return the totalAmount
	 */
	public Double getTotalAmount() {
		return totalAmount;
	}
	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 * @return the numberOfPayments
	 */
	public Long getNumberOfPayments() {
		return numberOfPayments;
	}
	/**
	 * @param numberOfPayments the numberOfPayments to set
	 */
	public void setNumberOfPayments(Long numberOfPayments) {
		this.numberOfPayments = numberOfPayments;
	}
	/**
	 * @return the regularPaymentAmount
	 */
	public Double getRegularPaymentAmount() {
		return regularPaymentAmount;
	}
	/**
	 * @param regularPaymentAmount the regularPaymentAmount to set
	 */
	public void setRegularPaymentAmount(Double regularPaymentAmount) {
		this.regularPaymentAmount = regularPaymentAmount;
	}
	/**
	 * @return the lastAmount
	 */
	public Double getLastAmount() {
		return lastAmount;
	}
	/**
	 * @param lastAmount the lastAmount to set
	 */
	public void setLastAmount(Double lastAmount) {
		this.lastAmount = lastAmount;
	}
	
	
}
