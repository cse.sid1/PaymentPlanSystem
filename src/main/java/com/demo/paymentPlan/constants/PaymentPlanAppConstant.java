package com.demo.paymentPlan.constants;

public class PaymentPlanAppConstant {

	
	
	public static final Integer VALIDATION_ERROR_CODE = 400;
	
	public static final Integer SUCCESS_CODE = 200;
	
	public static final Integer FAILURE_CODE = 501;
	
	public static final Integer ERROR_CODE = 99;
	
	public static final String USER_ERROR_MSG = "userErrorMsg";
	
	
	
	public static final String SUCCESS = "Success";

	public static final String FAILURE = "Failure";

	public static final String SUCCESS_STATUS = "S";

	public static final String FAIL_STATUS = "F"; 

	public static final String MESSAGE_REQUEST_ERROR = "messageRequestError";

}
