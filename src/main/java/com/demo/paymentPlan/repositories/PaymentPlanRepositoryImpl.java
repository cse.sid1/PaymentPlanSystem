package com.demo.paymentPlan.repositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.demo.paymentPlan.entity.PaymentPlanEntity;

@Repository
public class PaymentPlanRepositoryImpl {

	@PersistenceContext
    private EntityManager entityManager;
	
	@Transactional
	public void insertPaymentPlan(PaymentPlanEntity paymentPlanEntity) {
	    this.entityManager.persist(paymentPlanEntity);
	}
}
