package com.demo.paymentPlan.repositories;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.paymentPlan.entity.PaymentPlanEntity;

@Repository
public interface PaymentPlanRepository extends JpaRepository<PaymentPlanEntity, BigInteger> {


	
	@Query("SELECT p FROM PaymentPlanEntity p where p.paymentId = :paymentId")
	public PaymentPlanEntity getPaymentPlanById(@Param("paymentId") BigInteger paymentId );
	
	
	@Query("SELECT p FROM PaymentPlanEntity p")
	public List<PaymentPlanEntity> getAllPaymentPlans();
	
}
