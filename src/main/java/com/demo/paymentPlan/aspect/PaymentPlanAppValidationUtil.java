package com.demo.paymentPlan.aspect;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo.paymentPlan.constants.PaymentPlanAppConstant;
import com.demo.paymentPlan.exception.PaymentPlanAppException;

public class PaymentPlanAppValidationUtil extends PaymentPlanAppException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PaymentPlanAppValidationUtil.class);


	public void validate(Object obj) throws PaymentPlanAppException {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set < ConstraintViolation < Object >> constraintViolations = validator.validate(obj);
		if (constraintViolations != null && !constraintViolations.isEmpty()) {
			ConstraintViolation < Object > violation = constraintViolations.iterator().next();
			String message = violation.getPropertyPath() + " " + violation.getMessage();
			LOG.info("validate Request Json String in PaymentPlanAppValidationUtil:::"+ message);

			throw new PaymentPlanAppException(message, PaymentPlanAppConstant.VALIDATION_ERROR_CODE);
		}
	}


}
