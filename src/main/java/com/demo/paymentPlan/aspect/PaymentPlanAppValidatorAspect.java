package com.demo.paymentPlan.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.demo.paymentPlan.exception.PaymentPlanAppException;

@Aspect
@Component
public class PaymentPlanAppValidatorAspect {
	
	/**
	 * Validate event. 
	 * 
	 * @param joinPoint the join point 
	 * @throws PaymentPlanAppException 
	 * @throws Throwable                      the throwable 
	 */
	@Before("execution(* com.demo.paymentPlan.controller..*.*(..))")
	public void validateEvent(JoinPoint joinPoint) throws PaymentPlanAppException {
		final Object[] args = joinPoint.getArgs();
		PaymentPlanAppValidationUtil util;
		for (int i = 0; i < args.length; i++) {
			util = new PaymentPlanAppValidationUtil();
			util.validate(args[i]);
		}
	}

}
