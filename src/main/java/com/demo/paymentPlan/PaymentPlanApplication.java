package com.demo.paymentPlan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAutoConfiguration
@EnableTransactionManagement
@EntityScan({"com.demo.paymentPlan.entity"})
@ComponentScan({"com.demo.paymentPlan"})
@EnableJpaRepositories(basePackages = "com.demo.paymentPlan")
@SpringBootApplication
public class PaymentPlanApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentPlanApplication.class, args);
	}

}
