## PAYMENT PLAN SYSTEM

# CONTENTS OF THIS FILE
---------------------

 * Introduction
 * To Use : Import and Run
 * Technologies used 
 * Api call using Postman


# INTRODUCTION
------------

A project created to design a system with the following capabilities

a) Creation of a payment plan from supplied total amount and number of payments.
b) It should be possible to get a specific payment plan
c) It should be possible to get all payment plans existing in the system.


# TO USE : IMPORT AND RUN
------------

Just import the project as maven project and run as Java Application



# TECHNOLOGIES USED
-----------------

1. Java 8
2. Spring Boot 
3. H2 Database


# API CALL USING POSTMAN
----------------------

1. Create a Payment Plan
	
	URL : http://localhost:8090/createPaymentPlan
	
	Request Type : Post
	
	Request : {
				"totalAmount":10,
				"numberOfPayments":3
			  }
	
2. Get a specific payment plan

	URL : http://localhost:8090/getPaymentPlanByPaymentId/1

	Request Type : Get
		
	Response :
			  {
				"paymentId": 1,
				"totalAmount": 10.0,
				"numberOfPayments": 3,
				"regularPaymentAmount": 3.33,
				"lastAmount": 3.34
			  }
			  
			  
3. Get all payment plans existing in the system
	
	URL : http://localhost:8090/getAllPaymentPlans
		
	Request Type : Get
		
	Response :
				[
					{
						"paymentId": 1,
						"totalAmount": 10.0,
						"numberOfPayments": 3,
						"regularPaymentAmount": 3.33,
						"lastAmount": 3.34
					},
					{
						"paymentId": 2,
						"totalAmount": 5.444333222E9,
						"numberOfPayments": 1234,
						"regularPaymentAmount": 4411939.4,
						"lastAmount": 4411941.8
					}
				]
	
